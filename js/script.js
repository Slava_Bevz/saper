// 'use strict'
//------------------------------------------САПЕР----------------------------------------------

//---------------------------------Технические требования:-------------------------------------

//  Нарисовать на экране поле 8*8 (можно использовать таблицу или набор блоков).
//  Сгенерировать на поле случайным образом 10 мин. Пользователь не видит где они находятся.
//  Клик левой кнопкой по ячейке поля "открывает" ее содержимое пользователю.

//  Если в данной ячейке находится мина, игрок проиграл. В таком случае показать 
//  все остальные мины на поле. Другие действия стают недоступны, можно только 
//  начать новую игру.
//  Если мины нет, показать цифру - сколько мин находится рядом с этой ячейкой.
//  Если ячейка пустая (рядом с ней нет ни одной мины) - необходимо открыть все 
//  соседние ячейки с цифрами.

//  Клик правой кнопки мыши устанавливает или снимает с "закрытой" ячейки флажок мины.
//  После первого хода над полем должна появляться кнопка "Начать игру заново",  
//  которая будет обнулять предыдущий результат прохождения и заново инициализировать поле.
//  Над полем должно показываться количество расставленных флажков, и общее количество 
//  мин (например 7 / 10).

//  Не обязательное задание продвинутой сложности:

//  При двойном клике на ячейку с цифрой - если вокруг нее установлено такое же 
//  количество флажков, что указано на цифре этой ячейки, открывать все соседние ячейки.

//  Добавить возможность пользователю самостоятельно указывать размер поля. 
//  Количество мин на поле можно считать по формуле Количество мин = количество ячеек / 6.

//--------------------------------------------------------------------------------------------

const   tdWidthAndHeight = 35,
        tdMargin = 5,
        restartGame = 'Начать новую игру?',
        styleButton = 'border: 2px solid transparent; background-color: aqua; margin-bottom: 20px;',
        tdDefaultColor = 'aqua',
        bombShield = `background: aqua url('img/Antipirate.svg') no-repeat center;`;

let     mineFound = false,
        gameStart = false,
        firstMovement = false,
        flagPut = 0;

const buttonInint = document.getElementById('buttonGoInit');

buttonInint.addEventListener('click', () => {
    if(gameStart){
        return
    }
    const numRows = +sapperGo.value;  
    if( isNaN(numRows) || numRows == '' ){
        alert('Введіть цифру!');
        return;
    }
    const numFields = numRows * numRows;
    gameStart = true;
    document.querySelector('.flagToAppear').style.display = 'flex';
    const create = function(tag){
        return document.createElement(tag);
    }

    const section = document.getElementById('sapper-section'); 
    let tbody = create('tbody');
    section.append(tbody);

    for(let i = 1; i <= numRows; i++){
        let tr = create('tr');
        tbody.append(tr);

        for(let i = 1; i <= numRows; i++){
            let td = create('td');
            tr.append(td);
        } 
    } 
    section.style.cssText = ` 
        width: ${ numRows *  (tdWidthAndHeight + tdMargin*2) }px;
        height: ${ numRows * (tdWidthAndHeight + tdMargin*2) }px; 
    `;

    const   [...tdArr] = document.getElementsByTagName('TD'),   
            [...trArr] = document.getElementsByTagName('TR'), 
            numMines = Math.floor(numFields / 6),
            minesArr = [];

    while(minesArr.length < numMines){
        let randomNum = Math.floor( Math.random() *( tdArr.length - 0) + 0 );

        if ( !minesArr.includes(randomNum ) ){
            minesArr.push(randomNum);
        } 
    }
    const   flagPutDiv = document.querySelector('.flagPut'),
            flagAllDiv = document.querySelector('.flagAll'),
            flagAll = numMines;
    
    flagPutDiv.innerText = '0';
    flagAllDiv.innerText = `/${flagAll}`;
    minesArr.forEach( (item) => {
        tdArr[item].classList.remove('0');
        tdArr[item].classList.add('1');
    });
    const checkTr = function (indexClickedTr, indexInTr){  
        if(indexClickedTr < 0 || indexInTr <0 || indexClickedTr > numRows-1|| indexInTr > numRows-1){
            return  
        }else{
            const [...arrayOfTdCheck] = trArr[indexClickedTr].children;
            const checkTd = arrayOfTdCheck[indexInTr];
            const indexCheckedTd = tdArr.indexOf(checkTd);   
            return indexCheckedTd;   
        }
    } 

    tdArr.forEach(tdItem => {
        tdItem.style.cssText = `
            width: ${tdWidthAndHeight}px;
            height: ${tdWidthAndHeight}px; 
            margin: ${tdMargin}px;
            border:1px solid white;
            background-color: ${tdDefaultColor};
        `;
        if( !tdItem.classList.contains('1') ){
            tdItem.classList.add('0'); 
        }

        tdItem.addEventListener('click', function ( ) { 
            if( tdItem.classList.contains('flag') ){
                return;
            } 
            if( mineFound ){
                booooooo();
                return;
            }  
            const buttonsSection = document.getElementById('button_new_game');
            const resetButton = create('button');
            resetButton.setAttribute('type','button');
            resetButton.innerText = restartGame;
            resetButton.onmouseover = function(event) {
                let target = event.target;
                target.style.background = '#FF7E00';
            };
            resetButton.onmouseout = function(event) {
                let target = event.target;
                target.style.background = 'aqua';
            };
            resetButton.style.cssText = styleButton;
            if(firstMovement == false){
                buttonsSection.append(resetButton);
                firstMovement = true;
            }
            resetButton.addEventListener('click', function(){
                gameStart = false; 
                firstMovement = false;
                sapperGo.value = ''; 
                tbody.remove();
                resetButton.remove();
                flagPut = 0;
                flagPutDiv.innerText = flagPut;
                document.querySelector('.flagToAppear').style.display = 'none';
                mineFound = false;
            });            
            let mineCheck = tdItem.classList; 
            
            if( mineCheck.contains('1') ){ 
                minesArr.forEach( (item) => {
                    tdArr[item].style.cssText = bombShield;
                });
                mineFound = true;
            }else{
                const showMines = tdItem.getAttribute('data-minesAround');
                tdItem.innerText = showMines;
                if( showMines == 0){ 
                    const nearestTr = tdItem.closest('tr'), 
                        [...nearestTrCildrens] = nearestTr.children,
                        indexInTr = nearestTrCildrens.indexOf(tdItem),
                        indexClickedTr = trArr.indexOf(nearestTr); 
                    const checkTheMine = [
                        checkTr(indexClickedTr, indexInTr-1), 
                        checkTr(indexClickedTr, indexInTr+1), 
                        checkTr(indexClickedTr-1, indexInTr), 
                        checkTr(indexClickedTr-1, indexInTr-1), 
                        checkTr(indexClickedTr-1, indexInTr+1), 
                        checkTr(indexClickedTr+1, indexInTr), 
                        checkTr(indexClickedTr+1, indexInTr-1), 
                        checkTr(indexClickedTr+1, indexInTr+1)                    
                    ];
                    checkTheMine.forEach((el) => {  
                        if(el == undefined){
                            return 
                        }else{ 
                            if( tdArr[el].classList.contains('flag') ){
                                tdArr[el].classList.remove('flag');
                                flagPut--;
                                flagPutDiv.innerText = flagPut;
                            }
                            const showMines = tdArr[el].getAttribute('data-minesAround'); 
                            tdArr[el].innerText = showMines; 
                        } 
                    });        
                }
            } 
            setTimeout(booooooo, 50);              
        });
        
        tdItem.addEventListener('contextmenu', function(event){
            event.preventDefault();
            if( this.innerText ){ 
                return;
            }
            if( !mineFound ){
                if( this.classList.contains('flag') ){
                    this.classList.toggle("flag");
                    flagPut--;
                    flagPutDiv.innerText = flagPut;
                }else{
                    if( flagPut == flagAll ){
                        return
                    }else{
                        this.classList.toggle("flag");
                        flagPut++;
                        flagPutDiv.innerText = flagPut;
                    }
                }
            }
            booooooo();
        });
    });

    tdArr.forEach(tdItem => {
        const nearestTr = tdItem.closest('tr'),
            [...nearestTrCildrens] = nearestTr.children, 
            indexInTr = nearestTrCildrens.indexOf(tdItem),
            indexClickedTr = trArr.indexOf(nearestTr);
        const checkTheMine = [
            checkTr(indexClickedTr, indexInTr-1), 
            checkTr(indexClickedTr, indexInTr+1), 
            checkTr(indexClickedTr-1, indexInTr), 
            checkTr(indexClickedTr-1, indexInTr-1), 
            checkTr(indexClickedTr-1, indexInTr+1), 
            checkTr(indexClickedTr+1, indexInTr), 
            checkTr(indexClickedTr+1, indexInTr-1), 
            checkTr(indexClickedTr+1, indexInTr+1)                    
        ]
        let minesQuantity = 0;
        checkTheMine.forEach((el) => {    
            if( tdArr[el] == undefined ){
                return ;
            }else {
                if(tdArr[el].classList.contains('1')){
                    return minesQuantity++;
                }
            }
        });
        tdItem.setAttribute('data-minesAround', minesQuantity);
    });
});
function booooooo() {
    if (mineFound) {
        alert('Вы проиграли');
    }
};
